// find s in first name and d in last name
	// use $or
	// show only firstName and lastName, hide _id


db.users.find(
	{
		$or: [
			{"firstName": {$regex: 's', $options: "i"}},
			{"lastname": {$regex: 'd', $options: "i"}}
		]
	},
	{
			"firstName": 1,
			"lastName": 1,
      "_id": 0
	}
);


// find users who are from HR dept and age is $gte 70
	// use $and



	db.users.find(
	{
		$and: [
		{"department":"HR"},
		{"age": {$gte: 70}}
		]
	}
);
// alt solutions

db.users.find(
	{
		"department": "HR",
		"age": {$gte:70}
	}
);


// sol no.4
db.users.find(
	{
		$and: [
			{"firstName": {$regex: 'e', $options: 'i'}},
			{"age": {$lte: 30}}
		]
	}
);