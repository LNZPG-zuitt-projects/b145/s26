// look for docs that has age less than 50
db.collections.find({query}, {field projection})

db.users.find({
            "age": {$lt: 50}
      });


db.users.find({
            "age": {$gte: 50}
      });


db.users.find({
            "age": {$ne: 82}
      });


db.users.find({
            "lastName": {$in: ["Hawking", "Doe"]}
      });


// HTML and React


db.users.find({
            "courses": {$in: ["HTML", "React"]}
      });


// LOGICLA QUERY OPERATOR



// Neil or 25
db.users.find(
{
	$or: [
		{"firstName": "Neil"},
		{"age": 25}
	]
});



// Neil or >30
db.users.find(
{
	$or: [
		{"firstName": "Neil"},
		{"age": {$gt: 30}}
	]
});



// AND OPERAOR
db.users.find(
{
	$and: [
		 { "age": { $ne: 82 } },
		 { "age": { $ne: 76 } }
	]
});


// Field Projection

db.users.find(
	{
		"firstName": "Jane"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
);


// Hawking and exclude department fields
db.users.find(
	{
		"lastName": "Hawking"
	},
	{
		"contact": 0,
		"department": 0
	}
);


// Neil and exclude id but include first and last name and contact fields
db.users.find(
	{
		"firstName": "Neil"
	},
	{
		"_id": 0,
		"firstName": 1,
		"lastName": 1,
		"contact": 1
	}
);


// Bill and include first and last name and phone number fields
db.users.find(
	{
		"firstName": "Bill"
	},
	{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1,
		"_id": 0
	}
);


// Bill include everything except email.
db.users.find(
        {firstName: "Bill"},
        {
                "contact.email": 0
        }        
);



// Evaluation Query Operator
	// $regex operator

	db.users.find(
		{
			"firstName": {$regex: "N"}

		}
	);


	db.users.find(
		{
			"firstName": {$regex: "j"}

		}
	);


		db.users.find(
		{
			"firstName": {$regex: "n", $options: "i"}

		}
	);

